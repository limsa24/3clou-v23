# Créer une instance Linux Debian 12 

En utilisant la console Web de Scaleway :

- Créer une instance type PRO2-XXS (si ça passe...) avec
 une image Debian 12 et on va y installer Wordpress

- Dans la console prenez bien soin de sélectionner le
  projet 2024-VER-B3

- ON PRENDRA BIEN SOIN DE SUPPRIMER L'INSTANCE UNE FOIS
  LE TP terminé.

Cliquez sur le bouton "Créer une instance", caractéristiques :

- PRO2-XXS, Image OS : Debian 12
- Taille de volume par défaut
- Exécuter à partir de ce volume
- IP publique NAT
- NOTEZ BIEN le nom de l'instance : par ex : scw-modest-booth

Le point clef : la clef SSH

## Rappels sur SSH et la cryptographie à clef publique

* SSH permet de s'authentifier en utilisant la cryptographie
 au lieu d'un mot de passe
* De base SSH utilise la clef publique du serveur auquel on
 se connecte pour chiffrer le traffic (en réalité pour échanger
 une clef de session partagé qui chiffre le traffic)

Pour l'authentification des utilisateurs les plate-formes de
Clous (IaaS) ne permettent pas l'usage de mots de passes.

Principe de base de la cryptographie à clef publique ou 
assymétrique repose sur la génération (aléatoire) de deux
informations :

- Une clef privée (qui DOIT rester absolument privé)
- Une clef publique correspondante (elle PEUT être publié
 à tous)

Ce que l'on chiffre avec l'une de ces deux clefs se
déchiffre avec l'autre.

Conséquence 1 : si vous possédez une clef publique, vous
pouvez échanger secrètement avec le possesseur.se de la
clef privée. Seulement cette personne pourra lire le
message.

Conséquence 2 : si vous possédez une clef privée (normalement
vous êtes unique dans ce cas) tout possesseur de votre
clef publique peut déchiffrer vos messages. Donc à la 
limite tout le monde. Bof pour le secret. Mais vous prouvez
ainsi qui vous êtes.

C'est la conséquence 2 qui va nous permettre de prouvez que
nous sommes bien le propriétaire de l'instance.

## Créer et introduire une clef publique associée à l'instance

Dans votre système GNU/Linux local (ça pourrait être macOS ou
MS Windows), en ligne de commande, générez une paire de clefs :

~~~~Bash
$ ls ~/.ssh/id_rsa*
... si des noms de fichier apparaissent : dites le moi !
$ ssh-keygen -t rsa
(répondre entrée à toutes les questions)
$ ls -l ~/.ssh/id_rsa*
-rw-------  ... /home/.../.ssh/id_rsa
-rw-r--r--  ... /home/.../.ssh/id_rsa.pub
~~~~

Notez que la clef privée est protégée en lecture (seul son
propriétaire peut la lire).

Afficher sur le terminal le contenu de `id_rsa.pub` et de
le copier dans la console Web Scaleway avec un nom qui
vous identifie clairement.

~~~~Bash
$ cat ~/.ssh/id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDEVJ50W...
bWmGMrRIc1PJHkxuE6fF0mpdHWUGb5well6780Ff7Oxgq...
1US7dH4H7jYFCpoNpicwb6wfE8RCYUqNY5wK1N8raf0Ea...
15SzUE4CQ9vvnjk4qWTH1wfGA2qg6Ty9aKppBwfZvOksF...
q4GU1vOR2OsA5E3WMGeBrI/OWAV2RoCagkYNCh9GLNwGw...
... CtF2r58GtoWDFDIK7x userid@host
~~~~

Le bloc qui commence par ssh-rsa AAA... jusqu'à la fin.

L'opérateur de Cloud (Scaleway) va placer cette clef publique
à un endroit adapté (on verra où) de façon à ce que le possesseur
de la clef privée, et lui ou elle UNIQUEMENT (c'est à dire nous)
puisse se connecter à l'instance.

Note : Attention, la liste des clefs ne se met pas à jour sur
la page Web. On peut examiner la liste des clefs publiques
au niveau global du projet.

Une fois qu'elle a démarré on note son IP publique.

On peut tenter de connecter, à partir de notre système local :

~~~~Bash
$ ssh root@51.15.252.23
(avertissement sur l'authenticité de la clef publique d'instance,
c'est la clef publique aléatoire du serveur)
root@scw-modest-booth:~# whoami
root
root@scw-modest-booth:~# who
root     pts/0        2023-11-30 14:22 (46.247.227.178)
~~~~

Nous avons accès à notre instance en tant qu'administrateur
_(root)_.


