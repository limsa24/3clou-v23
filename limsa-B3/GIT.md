# Introduction minimale à GIT

GIT outil de gestion de version. 

Dépôt git : répertoire dans lequel il y a un sous-répertoire .git
("caché" sous UNIX/Linux, visible avec `ls -a`) qui contient 
l'historique des modification d'un certains nombre de fichiers.

Ces fichiers qui sont dit "suivis" (tracked) sont ceux que vous
créer et faites évoluer. Il ne faut PAS suivre des fichier qui
sont générés à partir de ceux-ci.

Comment initialiser un dépôt GIT :

Indépendamment de gitlab/github/etc : 

~~~~Bash
$ mkdir monprojet
$ cd monprojet
$ git init .
$ ls .git
c:\...> dir .git
~~~~

À partir d'un dépot créé sur gitlab/github/etc. : vous créez le
projet GIT sur la plateforme. Vous récupérer le lien de "clonage"
sur l'interface Web. Si c'est votre projet : prenez le lien ssh
(pensez à ajouter une clef publique à votre profil sur la plateforme
gitlab/github) ; si c'est le projet de quelqu'un d'autre qui
est public le lien https suffit.

~~~~Bash
(clonage d'un projet dont vous êtes membre)
$ git clone git@...
$ ls monprojet
(clonage d'un projet public)
$ git clone https://...
$ ls autreprojet
~~~~

Si vous avez déjà un couple de clef publique/privée sur votre
système, ajoutez la clef publique à votre profile gitlab/github.

Si vous voulez faire la même chose sous MS Windows : installer
git : https://gitforwindows.org/ (bash vient avec !) et vous
pouvez générer une paire de clef de la même façon que sous
Linux (`ssh-keygen -t rsa`) et ajouter le contenu du fichier
`...pub` dans votre profil gitlab/gitub.

Testez en clonant localement le projet avec son url SSH :

~~~~Bash
$ git clone git@..../nomduprojet
$ dir/ls
~~~~

## Commandes de base de GIT 

Avant les premières utilisations de GIT sur un compte
utilisateur donné il faut configurer son nom et son
mail (si vous l'oubliez il va vous le dire) :

~~~~Bash
$ git config user.name "Votre Nom"
$ git config user.email "votre mail"
~~~~

- `git add fichiers...` : demande à ce que ces fichiers soient
 suivi _(tracked)_
- `git commit fichiers...` ou `git commit -a -m "commentaires"` : enregistre
 dans la base GIT locale l'état courant des fichiers suivis
- `git push` : synchronise le projet sur son "origine" (gitlab/github/...)
 (envoie les modification via ssh à la plate-forme)
- `git pull` : récupère sur l'origine (gitlab/github/...) l'état du projet

Si vous modifiez l'arborescence de votre dépôt (renommage, déplacement
ou effacement de fichiers) il faut le faire à travers git et non avec les commandes
UNIX habituelles (mv et rm) : `git rm, git mv`.
