# Installation de Wordpress

Le but à ce stade est d'automatiser via un script Shell
l'installation de Wordpress dans une instance Scaleway.

On commence par le faire à la main :

https://www.linuxtuto.com/how-to-install-wordpress-on-debian-12/

(ou un autre tutorial)

Note : le nom du serveur qui apparaît dans le tutorial peut 
être remplacée par l'ip publique de votre serveur.

Voici l'architecture de Worpress (classique...) :

- Instance GNU/Linux
- Serveur Web (Apache2 ou NGINX)
- PHP (module pour Apache2 via cgi-mpm)
- Base de données relationnelle : typiquement MariaDB (fork de MySQL)
- Base initialisée en amont puis à la première connection à Wordpress 

Il est possible (à voir plus tard) d'avoir le frontal Web
et la base de données qui s'exécutent dans des intances
différentes.

- Assurez vous que vous pouvez accéder à votre instance par ssh,
 c'est-à-dire que `ssh root@ip_de_votre_instance` marche
 (sinon dites le moi)

- Une fois connecté à votre instance, suivez le tutorial ci-dessus,
 si un point du tutorial n'est pas clair : demandez moi

- Écrivez un script Shell (Bash) qui fait tout ça

  Note : lorsqu'un fichier de configuration doit être produit
  on peut s'appuyer sur git (lancé par le script !)

Notes d'installation :

Question posées par mysql_secure_installation :

- mdp de root (admin BDD) : rootpw
- Socket UNIX : Y (on n'a pas - pour l'instant besoin du réseau)
- Change root password : n
- Remove anonymous user : y 
- Disallow root login remotelly : y
- Remove test database : y

Le reste est "manuel" à traver un navigateur on va l'automatiser
en plus (il suffit de dump/import de la base wordpress_db)

Un souci : à un moment on doit récupérer l'adresse IP v4 

# Tester mon script

Détruisez votre instance et le volume système associé

Créez une nouvelle instance de même type (Debian 12, etc.)

~~~~Bash
$ cd /tmp
$ git clone https://gitlab.com/python_431/3-clou-v-23-jp.git
(ou git pull si vous l'avez déjà cloné)
$ cd 3-clou-v-23-jp/TP_wp
$ scp files/installwp root@ip_votre_instance:
$ ssh root@ip_votre_instance
root@votreinstance:$ ./installwp
~~~~

Testez en allant avec un navigateur sur http://ip_votre_instance

TODO : générer le block aléatoire dans le script
